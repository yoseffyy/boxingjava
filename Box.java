public class Box<T> {

    interface DidSetListener<T> {
        void didSet(T value);
    }

    interface BindListener<T> {
        void bind(T value);
    }

    private DidSetListener<T> didSetListener;
    private BindListener bindListener;

    private T value;

    public Box(T value) {
        this.didSetListener = new DidSetListener<T>() {
            @Override
            public void didSet(T value) {
                if (Box.this.bindListener != null) {
                    if (!Box.this.value.equals(value)) {
                        Box.this.value = value;
                        Box.this.bindListener.bind(Box.this.value);
                    }
                } else {
                    Box.this.value = value;
                }
            }
        };
        this.didSetListener.didSet(value);
    }

    public void setValue(T value) {
        this.didSetListener.didSet(value);
    }

    public void bind(BindListener<T> bindListener) {
        this.bindListener = bindListener;
    }
}
