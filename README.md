# Boxing Java

This class comes to give us the possibility to define a constant action for variables in our code once the value changes

# Exemple
====
```Java
// Variable declaration
Box<String> name = new Box<>("Yosef");

// Action declaration
name.bind(new Box.BindListener<String>() {
            @Override
            public void bind(String value) {
                System.out.println(value);
            }
        });

// Change the value
name.setValue("Menachem");

// same variable nothing happens
name.setValue("Shlomy");
name.setValue("Shlomy");
```

# Explanation

Every time we do setValue it activates the BindListener we defined in the bind and does the action that is there,
if the variable is the same variable then nothing happens.